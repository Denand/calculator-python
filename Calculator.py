# Калькулятор #

number_1 = int(input('Enter number 1: '))
operator = str(input('Enter operator ( only "+", "-", "*" and "/"): '))
number_2 = int(input('Enter number 2: '))
result = int(0)

if operator == '+':
    result = number_1 + number_2
    print(result)
elif operator == '-':
    result = number_1 - number_2
    print(result)
elif operator == '*':
    result = number_1 * number_2
    print(result)
elif operator == '/':
    if number_2 == 0:
        print('Error: Divide by zero.')
    else:
        result = number_1 / number_2
        print(result)
else:
    print('Error: Invalid operator.')